"""Файл содержит бизнес-логику заказа."""
from typing import Optional, Union

from fastapi import HTTPException
from sqlalchemy import select
from sqlalchemy.ext.asyncio import AsyncSession
from starlette import status

from app.models import Order, Customer
from app.schemas.order import OrderCreateSchema, OrderBaseSchema


async def create_order(db: AsyncSession, phone_number: str, new_order: OrderCreateSchema) -> Order:
    """
    Функция создает новый заказ в БД.

    :param phone_number: Номер телефона заказчика
    :param new_order: Принимает новые параметры заказа
    :param db: Подключение к БД
    :return: Новый заказ
    """
    customer = (await db.execute(select(Customer).where(Customer.phone_number == phone_number))).scalars_one_or_none()
    if customer.point_of_sale_id != new_order.point_of_sale_id:
        raise HTTPException(
            status_code=status.HTTP_400_BAD_REQUEST,
            detail='Invalid point_of_sale_id',
            headers={'WWW-Authenicate': 'Bearer'},
        )
    order = Order(**new_order.dict())
    db.add(order)
    await db.commit()
    return order


async def read_orders(db: AsyncSession, phone_number: str, order_id: Optional[int] = None) -> Union[None, Order, list[Order]]:
    """
    Функция возвращает список заказов из БД.

    :param phone_number: Номер телефона заказчика
    :param order_id: Принимает id заказа, как необязательный параметр
    :param db: Подключение к БД
    :return: Список заказов
    """

    if order_id:
        return (await db.execute(select(Order).where(Order.id == order_id))).scalar_one_or_none()

    return (await db.execute(select(Order))).scalars()


async def update_order(db: AsyncSession, phone_number: str, new_data: OrderBaseSchema, order_id: int) -> Optional[Order]:
    """
    Функция принимает новые данные и заменяет их в соответствие с id заказа в БД.

    :param phone_number: Номер телефона заказчика
    :param db:Подключение к базе данных
    :param new_data: Новые данные заказа
    :param order_id: Принимает id заказа
    :return: Изменненый заказ
    """
    order: Optional[Order] = (
        await db.execute(select(Order).where(Order.id == order_id))
    ).scalar_one_or_none()
    if order is None:
        return None
    order.date_created = new_data.date_created
    order.end_date = new_data.end_date
    order.point_of_sale_id = new_data.point_of_sale_id
    order.status = new_data.status
    order.customer_id = new_data.customer_id
    order.employee_id = new_data.employee_id
    await db.commit()
    return order


async def delete_orders(db: AsyncSession, phone_number: str,order_id: int) -> Optional[bool]:
    """
    Фукция удаляет запись из БД.

    :param phone_number: Номер телефона заказчика
    :param db: Подключение к БД
    :param order_id: Принимает id заказа
    :return: True, False, None
    """
    delete_order = (
        await db.execute(select(Order).where(Order.id == order_id))
    ).scalar_one_or_none()
    if delete_order is None:
        return None
    await db.delete(delete_order)
    await db.commit()
    return True
