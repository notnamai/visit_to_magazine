"""Файл содержит бизнес-логику посещения."""
from typing import Optional, Union

from sqlalchemy import select
from sqlalchemy.ext.asyncio import AsyncSession

from app.models import Visit
from app.schemas.visit import VisitCreateSchema, VisitBaseSchema


async def create_visit(db: AsyncSession, new_visit: VisitCreateSchema) -> Visit:
    """
    Функция создает новый заказ в БД.

    :param new_visit: Принимает новые параметры посещения
    :param db: Подключение к БД
    :return: Новое посещение
    """
    visit = Visit(**new_visit.dict())
    db.add(visit)
    await db.commit()
    return visit


async def read_visits(db: AsyncSession, visit_id: Optional[int] = None) -> Union[None, Visit, list[Visit]]:
    """
    Функция возвращает список посещений из БД.

    :param visit_id: Принимает id посещения, как необязательный параметр
    :param db: Подключение к БД
    :return: Список посещений
    """

    if visit_id:
        return (await db.execute(select(Visit).where(Visit.id == visit_id))).scalar_one_or_none()

    return (await db.execute(select(Visit))).scalars()


async def update_visit(db: AsyncSession, new_data: VisitBaseSchema, visit_id: int) -> Optional[Visit]:
    """
    Функция принимает новые данные и заменяет их в соответствие с id посещения в БД.

    :param db: Подключение к базе данных
    :param new_data: Новые данные посещения
    :param visit_id: Принимает id посещения
    :return: Изменненое посещение
    """
    visit: Optional[Visit] = (
        await db.execute(select(Visit).where(Visit.id == visit_id))
    ).scalar_one_or_none()
    if visit is None:
        return None
    visit.employee_id = new_data.employee_id
    visit.order_id = new_data.order_id
    visit.customer_id = new_data.customer_id
    visit.point_of_sale_id = new_data.point_of_sale_id
    await db.commit()
    return visit


async def delete_visits(db: AsyncSession, visit_id: int) -> Optional[bool]:
    """
    Фукция удаляет запись из БД.

    :param db: Подключение к БД
    :param visit_id: Принимает id посещения
    :return: True, False, None
    """
    delete_visit = (
        await db.execute(select(Visit).where(Visit.id == visit_id))
    ).scalar_one_or_none()
    if delete_visit is None:
        return None
    await db.delete(delete_visit)
    await db.commit()
    return True
