"""Файл содержит бизнес-логику работника"""
from typing import Optional, Union

from sqlalchemy import select
from sqlalchemy.ext.asyncio import AsyncSession

from app.models.employee import Employee
from app.schemas.employee import EmployeeCreateSchema


async def create_employee(db: AsyncSession, new_employee: EmployeeCreateSchema) -> Employee:
    """
    Функция создает нового работника в БД.

    :param new_employee: Принимает новыве параметры работника
    :param db: Подключение к БД
    :return: Нового работника
    """
    employee = Employee(**new_employee.dict())
    db.add(employee)
    await db.commit()
    return employee


async def read_employees(db: AsyncSession, employee_id: Optional[int] = None) -> Union[None, Employee, list[Employee]]:
    """
    Функция возвращает список работников из БД.

    :param employee_id: Принимает id работника, как необязательный параметр
    :param db: Подключение к БД
    :return: Список работников
    """
    if employee_id:
        return (await db.execute(select(Employee).where(Employee.id == employee_id))).scalar_one_or_none()

    return (await db.execute(select(Employee))).scalars()

