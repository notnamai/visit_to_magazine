"""Файл содержит бизнес-логику заказчика."""

from sqlalchemy.ext.asyncio import AsyncSession

from app.models.customer import Customer
from app.schemas.customer import CustomerCreateSchema


async def create_customer(db: AsyncSession, new_customer: CustomerCreateSchema) -> Customer:
    customer = Customer()