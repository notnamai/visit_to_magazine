"""Файл содержит бизнес-логику торговой точки."""

from typing import Union, Optional

from sqlalchemy import select
from sqlalchemy.ext.asyncio import AsyncSession

from app.models import Employee
from app.models.point_of_sale import PointOfSale
from app.schemas.point_of_sale import PointOfSaleCreateSchema


async def create_point_of_sale(db: AsyncSession, new_point_of_sale: PointOfSaleCreateSchema) -> PointOfSale:
    """
    Функция создает новую торговую точку в БД.

    :param db: Подключение к БД
    :param new_point_of_sale: Название новой торговой точки
    :return: Новую торговую точку
    """
    point_of_sale = PointOfSale(name=new_point_of_sale)
    db.add(point_of_sale)
    await db.commit()
    return point_of_sale


async def read_point_of_sale(db: AsyncSession, phone_number: str, name: Optional[str] = None, point_of_sale_id: Optional[int] = None) -> Union[None, PointOfSale, list[PointOfSale]]:
    """
    Функция возвращает список торговых точек из БД.

    :param name: Название торговой точки
    :param phone_number: Телефонный номер работника
    :param point_of_sale_id: Принимает id торговой точки, как необязательный параметр
    :param db: Подключение к БД
    :return: Список торговывх точек
    """
    query = select(PointOfSale).join(
        Employee, Employee.point_of_sale_id == PointOfSale.id,
    ).where(Employee.phone_number == phone_number)
    if name is not None:
        query = query.where(PointOfSale.name.ilike(f'%{name}%'))
    if point_of_sale_id:
        return (await db.execute(query.where(PointOfSale.id == point_of_sale_id))).scalars_one_or_none()

    return (await db.execute(query)).scalars()

