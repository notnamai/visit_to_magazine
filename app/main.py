"""Файл содержит главные узлы приложения."""


from fastapi import FastAPI

from app.models.database import recreate_db
from app.views import point_of_sale, employee


app = FastAPI()


@app.on_event("startup")
async def startup():
    recreate_db()

app.include_router(point_of_sale.router)
app.include_router(employee.router)

if __name__ == "__main__":
    import uvicorn
    uvicorn.run(app, host='0.0.0.0', port=5466)

