"""Файл содержит эндпоинты торговых точек."""
from typing import Optional

from fastapi import APIRouter, Depends
from sqlalchemy.ext.asyncio import AsyncSession

from app.crud.point_of_sale import read_point_of_sale, create_point_of_sale
from app.models.database import get_db
from app.schemas.point_of_sale import PointOfSaleSchema, PointOfSaleCreateSchema

router = APIRouter(prefix='/point_of_sale', tags=['PointOfSale'])

dep_get_db = Depends(get_db)


@router.post('/', response_model=PointOfSaleSchema)
async def point_of_sale_post_list(new_point_of_sale: PointOfSaleCreateSchema, db: AsyncSession = dep_get_db):
    """
    Функция позволяет создать новую торговую точку.

    :param new_point_of_sale: Торговая точка
    :param db: Подключение к БД
    :return: Создание новой торговой точки
    """
    point_of_sale = await create_point_of_sale(db, new_point_of_sale)
    return PointOfSaleSchema.from_orm(point_of_sale)


@router.get('/', response_model=PointOfSaleSchema)
async def point_of_sale_get_list(phone_number: str, name: Optional[str] = None, db: AsyncSession = dep_get_db):
    """
    Функция возвращает список торговых точек.

    :param name: Название торговой точки
    :param phone_number: Телефонный номер работника
    :param db: Подключение к БД
    :return: Список торговых точек
    """
    point_of_sale = await read_point_of_sale(db, phone_number, name)
    return [PointOfSaleSchema.from_orm(i_point_of_sale) for i_point_of_sale in point_of_sale]
