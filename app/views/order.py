"""Файл содержит эндпоинты заказов."""

from fastapi import APIRouter, Depends, HTTPException
from sqlalchemy.ext.asyncio import AsyncSession

from app.crud.order import create_order, read_orders, update_order, delete_orders
from app.models.database import get_db
from app.schemas.order import OrderSchema, OrderCreateSchema, OrderBaseSchema


router = APIRouter(prefix='/order', tags=['Order'])

dep_get_db = Depends(get_db)
not_found_error = 404


@router.post('/', response_model=OrderSchema)
async def order_post_list(new_order: OrderCreateSchema, phone_number: str, db: AsyncSession = dep_get_db):
    """
    Функция позволяет создать новую торговую точку.

    :param phone_number:
    :param new_order: Новый заказ
    :param db: Подключение к БД
    :return: Создание нового заказа
    """
    order = await create_order(db, phone_number, new_order)
    return OrderSchema.from_orm(order)


@router.get('/', response_model=OrderSchema)
async def order_get_list(phone_number: str, db: AsyncSession = dep_get_db):
    """
    Функция возвращает список заказов.

    :param db: Подключение к БД
    :return: Список заказав
    """
    order = await read_orders(db, phone_number)
    return [OrderSchema.from_orm(i_order) for i_order in order]


@router.get('/{order_id}', response_model=OrderSchema)
async def order_get_detail(order_id: int, phone_number: str,db: AsyncSession = dep_get_db):
    """
    Функция возвращает одну запись о заказе в соответствие с id этой записи.

    :param phone_number:
    :param order_id: Id записи заказа
    :param db: Подкючение  к БД
    :return: Одну запись о заказе
    """
    order = await read_orders(db, phone_number, order_id=order_id)
    if order is None:
        raise HTTPException(status_code=not_found_error, detail='Order not found')
    return OrderSchema.from_orm(order)


@router.put('/{order_id}', response_model=OrderSchema)
async def products_balance_put_detail(
    order_id: int, new_data: OrderBaseSchema, phone_number: str, db: AsyncSession = dep_get_db,
):
    """
    Функция принимает новые данные и заменяет их в соответствие с id записи заказа.

    :param phone_number:
    :param order_id: Id записи заказа
    :param new_data: Новые данные заказа
    :param db: Подкючение  к БД
    :return: Новые данные заказа
    """
    order = await update_order(db, phone_number, new_data, order_id)
    if order is None:
        raise HTTPException(status_code=not_found_error, detail='Order not found')
    return OrderSchema.from_orm(order)


@router.delete('/{order_id}', response_model=str)
async def products_price_delete_detail(order_id: int, phone_number: str, db: AsyncSession = dep_get_db):
    """
    Фукция удаляет запись о цене товара.

    :param phone_number:
    :param order_id: Id записи заказа
    :param db: Подключение к БД
    :return: Подтверждение об удалении записи
    """
    order = await delete_orders(db, phone_number, order_id)
    if order is None:
        raise HTTPException(status_code=not_found_error, detail='Order not found')
    return 'Order delete'
