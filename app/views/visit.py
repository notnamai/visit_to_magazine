"""Файл содержит эндпоинты посещения."""

from fastapi import APIRouter, Depends, HTTPException
from sqlalchemy.ext.asyncio import AsyncSession

from app.crud.visit import create_visit, read_visits, update_visit, delete_visits
from app.models.database import get_db
from app.schemas.visit import VisitSchema, VisitCreateSchema, VisitBaseSchema


router = APIRouter(prefix='/visit', tags=['Visit'])

dep_get_db = Depends(get_db)
not_found_error = 404


@router.post('/', response_model=VisitSchema)
async def order_post_list(new_visit: VisitCreateSchema, db: AsyncSession = dep_get_db):
    """
    Функция позволяет создать новое посещение.

    :param new_visit: Новое посещение
    :param db: Подключение к БД
    :return: Создание нового посещения
    """
    visit = await create_visit(db, new_visit)
    return VisitSchema.from_orm(visit)


@router.get('/', response_model=VisitSchema)
async def order_get_list(db: AsyncSession = dep_get_db):
    """
    Функция возвращает список посещений.

    :param db: Подключение к БД
    :return: Список посещений
    """
    visit = await read_visits(db)
    return [VisitSchema.from_orm(i_visit) for i_visit in visit]


@router.get('/{visit_id}', response_model=VisitSchema)
async def order_get_detail(visit_id: int, db: AsyncSession = dep_get_db):
    """
    Функция возвращает одну запись о посещении в соответствие с id этой записи.

    :param visit_id: Id записи посещения
    :param db: Подкючение  к БД
    :return: Одну запись о посещении
    """
    visit = await read_visits(db, visit_id=visit_id)
    if visit is None:
        raise HTTPException(status_code=not_found_error, detail='Visit not found')
    return VisitSchema.from_orm(visit)


@router.put('/{visit_id}', response_model=VisitSchema)
async def products_balance_put_detail(
    visit_id: int, new_data: VisitBaseSchema, db: AsyncSession = dep_get_db,
):
    """
    Функция принимает новые данные и заменяет их в соответствие с id записи посещения.

    :param visit_id: Id записи посещения
    :param new_data: Новые данные посещения
    :param db: Подкючение  к БД
    :return: Новые данные посещения
    """
    visit = await update_visit(db, new_data, visit_id)
    if visit is None:
        raise HTTPException(status_code=not_found_error, detail='Visit not found')
    return VisitSchema.from_orm(visit)


@router.delete('/{visit_id}', response_model=str)
async def products_price_delete_detail(visit_id: int, db: AsyncSession = dep_get_db):
    """
    Фукция удаляет запись о цене товара.

    :param visit_id: Id записи посещения
    :param db: Подключение к БД
    :return: Подтверждение об удалении записи
    """
    visit = await delete_visits(db, visit_id)
    if visit is None:
        raise HTTPException(status_code=not_found_error, detail='Visit not found')
    return 'Visit delete'
