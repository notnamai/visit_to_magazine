from fastapi import APIRouter, Depends
from sqlalchemy.ext.asyncio import AsyncSession

from app.crud.employee import create_employee
from app.models.database import get_db
from app.schemas.employee import EmployeeSchema, EmployeeCreateSchema

router = APIRouter(prefix='/employee', tags=['Employee'])

dep_get_db = Depends(get_db)


@router.post('/', response_model=EmployeeSchema)
async def employees_post_list(new_employee: EmployeeCreateSchema, db: AsyncSession = dep_get_db):
    """
    Функция позволяет создать нового работника.

    :param new_employee: Данные работника
    :param db: Подключение к БД
    :return: Создание нового работника
    """
    employee = await create_employee(db, new_employee)
    return EmployeeSchema.from_orm(employee)
