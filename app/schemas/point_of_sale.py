"""Файл схемы торговой точки."""
from pydantic import BaseModel


class PointOfSaleBaseSchema(BaseModel):

    name: str


class PointOfSaleCreateSchema(PointOfSaleBaseSchema):

    pass


class PointOfSaleSchema(PointOfSaleBaseSchema):

    id: int

    class Config:
        orm_mode = True
