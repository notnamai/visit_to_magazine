"""Файл схемы посещение."""
from datetime import datetime

from pydantic import BaseModel


class OrderBaseSchema(BaseModel):

    date_created: datetime
    end_date: datetime
    point_of_sale_id: int
    status: str
    customer_id: int
    employee_id: int


class OrderCreateSchema(OrderBaseSchema):

    pass


class OrderSchema(OrderBaseSchema):

    id: int

    class Config:
        orm_mode = True
