"""Файл схемы работника."""
from pydantic import BaseModel


class EmployeeBaseSchema(BaseModel):

    name: str
    point_of_sale_id: int
    phone_number: str


class EmployeeCreateSchema(EmployeeBaseSchema):

    pass


class EmployeeSchema(EmployeeBaseSchema):

    id: int

    class Config:
        orm_mode = True
