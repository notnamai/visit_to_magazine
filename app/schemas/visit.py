"""Файл схемы посещение."""
from datetime import datetime

from pydantic import BaseModel


class VisitBaseSchema(BaseModel):
    date_created: datetime
    employee_id: int
    order_id: int
    customer_id: int
    point_of_sale_id: int


class VisitCreateSchema(VisitBaseSchema):

    pass


class VisitSchema(VisitBaseSchema):

    id: int

    class Config:
        orm_mode = True
