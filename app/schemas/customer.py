"""Файл схемы заказчика."""
from pydantic import BaseModel


class CustomerBaseSchema(BaseModel):

    name: str
    point_of_sale_id: int
    phone_number: str


class CustomerCreateSchema(CustomerBaseSchema):

    pass


class CustomerSchema(CustomerBaseSchema):

    id: int

    class Config:
        orm_mode = True
