"""Файл модели посещение"""

from sqlalchemy import (
    Column,
    Integer,
    ForeignKey, DateTime,
)

from app.models.database import Base


class Visit(Base):
    """Модель содержит имя работника, номер заказа, имя заказчика, название торговой точки."""

    __tablename__ = 'visit'

    id = Column(Integer, primary_key=True)
    date_created = Column(DateTime(timezone=True))
    employee_id = Column(Integer, ForeignKey('employee.id', ondelete='CASCADE'), nullable=False)
    order_id = Column(Integer, ForeignKey('order.id', ondelete='CASCADE'), nullable=False)
    customer_id = Column(Integer, ForeignKey('customer.id', ondelete='CASCADE'), nullable=False)
    point_of_sale_id = Column(Integer, ForeignKey('point_of_sale.id', ondelete='CASCADE'), nullable=False)
