"""Файл модели работника."""

from sqlalchemy import (
    Column,
    Integer,
    String, ForeignKey,
)
from sqlalchemy.orm import relationship

from app.models.database import Base


class Employee(Base):
    """Модель содержит имя сотрудника, название торговой точки, номер телефона."""

    __tablename__ = 'employee'

    id = Column(Integer, primary_key=True)
    name = Column(String)
    point_of_sale_id = Column(Integer, ForeignKey('point_of_sale.id', ondelete='CASCADE'), nullable=False)
    point_of_sale = relationship('PointOfSale', foreign_keys=[point_of_sale_id])
    phone_number = Column(String)


