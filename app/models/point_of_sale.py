"""Файл модели торговой точки."""
from typing import List, TYPE_CHECKING

from sqlalchemy import (
    Column,
    Integer,
    String,
)
from sqlalchemy.orm import relationship

from app.models.database import Base
if TYPE_CHECKING:
    from app.models.employee import Employee

class PointOfSale(Base):
    """Модель содержит название торговой точки."""

    __tablename__ = 'point_of_sale'

    id = Column(Integer, primary_key=True)
    name = Column(String)
    employee: List['Employee'] = relationship(
        "Employee",
        cascade="save-update,merge,delete,delete-orphan",
        back_populates='point_of_sale'
    )

