"""Файл содержит подключение к базе данных."""
from sqlalchemy import create_engine
from sqlalchemy.engine import make_url
from sqlalchemy.ext.asyncio import (
    AsyncSession,
    create_async_engine,
    )
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import sessionmaker
from sqlalchemy_utils import database_exists, create_database

Base = declarative_base()


def create_pool(
    pool_size: int = 20, uri: str = 'postgresql+asyncpg://postgres:postgres@127.0.0.1:5461/visit_to_magazine',
) -> sessionmaker:
    """
    Функция принимает адрес поключение к базе данных и количество пулов.

    :param pool_size: количество пулов
    :param uri: адрес подключения к БД
    :return: Готовое подключение
    """
    engine = create_async_engine(url=make_url(uri), pool_size=pool_size, max_overflow=0)
    pool = sessionmaker(bind=engine, class_=AsyncSession, expire_on_commit=False, autoflush=False)
    return pool


pool = None


async def get_db():
    global pool
    if pool is None:
        pool = create_pool()
    try:
        async with pool() as session:
            yield session
    finally:
        if session:
            await session.close()


def recreate_db():
    uri = "postgresql://{login}:{password}@{host}:{port}/{db}".format(
        login='postgres',
        password='postgres',
        host='127.0.0.1',
        port=5461,
        db='visit_to_magazine',
    )
    #logger.debug("Recreate DB..")
    engine = create_engine(uri)

    if not database_exists(engine.url):
        print("hello")
        create_database(engine.url)
