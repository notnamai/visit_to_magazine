"""Файл модели заказа."""

from sqlalchemy import (
    Column,
    Integer,
    String, ForeignKey, DateTime,
)

from app.models.database import Base


class Order(Base):
    """Модель содержит дату создания заказа, дата окончания заказа, название торговой точки,
     статус заказа, имя заказчика, имя исполнителя."""

    __tablename__ = 'order'

    id = Column(Integer, primary_key=True)
    date_created = Column(DateTime(timezone=True))
    end_date = Column(DateTime(timezone=True))
    point_of_sale_id = Column(Integer, ForeignKey('point_of_sale.id', ondelete='CASCADE'), nullable=False)
    status = Column(String)
    customer_id = Column(Integer, ForeignKey('customer.id', ondelete='CASCADE'), nullable=False)
    employee_id = Column(Integer, ForeignKey('employee.id', ondelete='CASCADE'), nullable=False)
