"""Файл модели заказчика."""

from sqlalchemy import (
    Column,
    Integer,
    String, ForeignKey,
)

from app.models.database import Base


class Customer(Base):
    """Модель содержит имя заказчика, название торговой точки, номер телефона."""

    __tablename__ = 'customer'

    id = Column(Integer, primary_key=True)
    name = Column(String)
    point_of_sale_id = Column(Integer, ForeignKey('point_of_sale.id', ondelete='CASCADE'), nullable=False)
    phone_number = Column(String)


